# Exercicis Disseny de bases de dades

Per a tots els enunciats que es plantegen s'ha de fer:

+ Model Entitat relació

+ Model relacional

+ Script de creació de la base de dades

## Exercici 1

**L'hospital**

A l'hospital de Sant Delete hi visiten metges (que es caracteritzen per un DNI
i tenen nom i cognoms), que atenen pacients (que tenen un codi, nom i cognoms),
els quals tenen malalties (que codifiquem amb un nombre i que tenen un nom
tècnic i un nom comú).

## Exercici 2

**Botiga d'informàtica**

Es vol modelitzar el procés de venda de productes a una botiga d'informàtica.
Els seus productes tenen un codi de barres i un preu associat. A la botiga, hi
despatxen 2 empleats, els quals tenen DNI i nom. La botiga també disposa de
proveïdors que es caracteritzen per un nom i tenen un número de telèfon.

## Exercici 3

**Missatgeria**

A una certa empresa de missatgeria hi ha dos tipus de treballadors: missatgers
i administratius.

+ Els missatgers tenen motos associades, i els administratius, ordinadors.

+ Els administratius reben els paquets dels clients i els donen als missatgers
  associats a la zona on va destinat el paquet.

+ Els missatgers agafen una moto i porten el paquet al destinatari.

+ Els paquets tenen un pes associat i una adreça de destinació.

+ Les motos tenen un nombre identificatiu i una cilindrada.

+ El destinatari té un nom i una adreça associats.

## Exercici 4

**Immobiliària**

El gerent d'una immobiliària ens ha explicat que el seu sistema
d’informació ha d’incloure les dades de tots els habitatges que gestiona:

+ De cada habitatge cal saber-ne:

	- el tipus (pis, àtic, casa aïllada, casa, loft, local comercial, magatzem, etc.)

	- l’adreça completa (tipus de via -que pot ser carrer, avinguda, rambla,
	  carretera, ronda, etc.-, nom de la via, número, bloc, pis, porta, codi
postal, població, municipi, província, país),

	- les característiques de l’habitatge (número d’habitacions, número de
	  banys, si té o no balcó o terrassa, si inclou o no plaça d’aparcament, si
té o no traster, si té o no safareig, si té o no pati, si disposa de calefacció
i/o aire condicionat i d’altres característiques que puguin ser interessants)

	- el preu de venda o de lloguer mensual.

	- També cal enregistrar si l’habitatge està destinat al lloguer o a la compra

	- la referència al seu propietari (que suposarem únic per cada habitatge). 

+ A cada habitatge cal associar-li les dades de la persona de contacte que
  l’ofereix en lloguer o per vendre. 

+ Dels propietaris i de les persones de contacte cal tenir-ne la següent
  informació: NIF, nom i cognoms, telèfon de contacte i adreça de correu
electrònic de contacte. 

+ En la base de dades es registraran, també, les dades dels comercials de la
  immobiliària, en concret: el seu NIF, nom i cognoms, telèfon mòbil
proporcionat per l’empresa, telèfons (almenys 1) particulars, adreça de correu
electrònic, adreça postal completa i número de compte per a rebre les nòmines. 

+ També cal emmagatzemar les dades dels clients que visiten els habitatges.
  Així doncs, caldrà demanar-los les següents dades: NIF, nom, cognoms, adreça,
telèfons (almenys un) de contacte i adreça de correu electrònic. 

+ De cada client, caldrà saber quins habitatges ha visitat, en quina data i a
  través de quin comercial. 

+ De cada habitatge que s’ha venut o llogat cal saber per part de quin client,
  a través de quin comercial i en quina data. 

+ La immobiliària també desitja guardar la informació de les nòmines dels seus
  comercials. En concret, cal guardar la següent informació: nom, cognoms i NIF
del comercial, data de pagament, compte de pagament i totalitat de l’import
abonat. A més cal enregistrar desglossadament l’import de la part de la nòmina
que correspon al sou base, dels diversos plus que pugui generar el comercial
(productivitat, premis, etc.) i de les diverses comissions que pugui obtenir de
la venda d’habitatges. 

## Exercici 5

**Pizzeria**

+ La cadena de pizzeries té una carta de pizzes on cada pizza conté diversos
  ingredients. 

+ Cada ingredient d’una pizza pot ser substituït per altres ingredients, en cas
  d’inexistència. 

+ La cadena de pizzeries té locals que poden ser de tipus restaurant, on els
  clients poden degustar-hi les pizzes in situ, o de tipus “per emportar”. Un
mateix local pot ser, a la vegada, restaurant i admetre comandes per emportar. 

+ Per cada comanda, en una pizzeria, cal enregistrar les línies que la
  composen, seguin com a mostra el següent exemple: 

	- COMANDA 278 2 Pizza margarita 10 €/unitat 1 Pizza americana 12 €/unitat 3
	  Refrescos 3 €/unitat TOTAL: 41 € 

+ Cada local de la pizzeria té assignats diversos empleats, que poden ser:
  cuiners, cambrers, telefonistes o motoristes. Tenint en compte, però, que els
un empleat pot canviar de rol, en un moment donat. Per exemple, un cambrer pot
ser telefonista en un moment donat. 

+ Cal enregistrar en el sistema de bases de dades, quin empleat serveix cada
  comanda, en una pizzeria, per tal de poder obtenir a posteriori l’empleat del
mes (aquell que ha facturat més). 

+ En una pizzeria, cada empleat motorista té associada una moto, però una moto
  és compartida per diversos motoristes de diversos torns. 

+ En cada local d’una cadena de pizzeries cal controlar l’stock de cada
  ingredient que hi ha en un moment donat, així com l’stock mínim admissible,
per tal que el sistema doni l’avís de compra, si es passa aquest mínim. 

+ En els locals que són de tipus restaurant, hi ha diverses taules. De cada
  taula cal enregistrar en la nostra base de dades el nombre de seients. 

+ En els locals de tipus restaurant s’admeten reserves. Llavors caldrà
  enregistrar el nom, el telèfon, el nombre de persones i la data i hora de la
reserva, a més de la taula que se’ls assignarà, en el moment de fer la reserva. 

## Ampliació de Teoria

**Definicions**

Defineix i posa un exemple de:

1. Atribut simple

2. Atribut compost

3. Atribut monovaluat

4. Atribut multivaluat

5. Atribut derivat

6. Entitat feble
