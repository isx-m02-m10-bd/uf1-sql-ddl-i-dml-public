# pgmodeler: instal·lació i configuració

## Instal·lació a Debian estable

Executem com a root l'ordre:
```
# apt-get install pgmodeler
```

## Configuració de la nostra base de dades

Editem `/etc/postgresql/13/main/pg_hba.conf` per poder atacar per TCP/IP la nostra màquina local:

```
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
```

Reiniciem postgresql:

```
# systemctl restart postgresql
```

Si el nostre usuari no té contrasenya al nostre sistema gestor de base de dades li posem:

```
$ su  -l postgres
Password: 
$ psql template1
...
template1=# ALTER USER el_vostre_usuari PASSWORD 'postgres';
ALTER ROLE
```

Executem pg modeler i anem a *Settings*. Creem una nova connexió local amb les nostres dades:

![Configuració de la connexió](imatges/pgmodeler_settings_connexio.png)

